# Python 101 (w GitLab) - Metz University 
# Instructor: Pj Metz

## Introduction:
This intro course is designed to focus on skills that are necessary for writing and debugging code in Python. We are not focused on theory as much as immediately applicable skills for students to use in building apps and programs, thus work will take place utilizing similar tools that current companies use when writing code. These programs will be simple and eventually move to more complex as the semester progresses. The final project is a group project completed on GitLab where students will create a program that works with the API of your choosing. There is no written midterm or final.

## Location and Time:
Edward Metz Hall, Room 13. 
Tues/Thurs 1:20 - 2:50

## Canvas: 
Course information is available on Canvas, but all communication, work, and assignments are turned in on GitLab. *There is no dropbox for assignments on Canvas for this course, and assignments submitted through other venues will be ignored.* 

## Course Objectives:
Students will be able to:
Move from ideation to conceptualization to product using Python
Write code that adheres to PEP8 standards
Read code written by other developers
Identify bugs or inefficient code in simple Python files
Use simple Git commands correctly to manage source control
Navigate GitLab UI for efficient collaboration to achieve group goals
Work with a public API 
Identify outside tools, libraries, and frameworks that can be used to assist in achieving group goals

## Technology:
Please bring your school Macbook with you to class everyday along with a charger. Each class is equipped with desks with multiple outlets for students to plug into. Mechanical Keyboards are considered noisy and are not preferable for the classroom, either use a “noiseless keyboard” or the laptop keyboard. 

## GitLab:
You have been added to a group in GitLab based on your email address. This account is free thanks to GitLab for Education. You will have access to the Ultimate license of the platform. We will cover GitLab UI navigation on the first day of class. If you’d like to look ahead, you can watch this [Youtube video](https://www.youtube.com/watch?v=-oaI2WEKdI4) that covers the “Learn GitLab” issue that is automatically assigned to new users.
